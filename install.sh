#! /usr/bin/env sh
echo "Create Python virtual environment..."
python3 -m venv venv
./venv/bin/activate
echo "Upgrading pip..."
python -m pip install --upgrade pip
echo "Installing dependencies..."
pip install -r requirements.txt
