FROM python:3.10

RUN mkdir -p usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/

RUN pip install --upgrade pip
RUN apt-get update

RUN pip install --no-cache-dir -r ../requirements.txt

CMD [ "python", "./server.py" ]
