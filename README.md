# Draft Server

## Installation

### Docker

```bash
git clone --recurse-submodules https://gitlab.com/nicolas_capon/draft-server.git
cd draft-server
docker-compose up --build draft-server
```


### Local installation

1) Install Git and Python (> 3.8)
```powershell
# For Windows on PowerShell
winget install -e --id Git.Git
winget install -e --id Python.Python.3.10
```

2) Clone this repository
```bash
git clone --recurse-submodules https://gitlab.com/nicolas_capon/draft-server.git
```

3) Run the install script or go to step 4 to continue the manual installation
* For Windows PowerShell
```powershell
.\install.ps1
```
* For Linux
```bash
chmod +x install.sh && ./install.sh
```

4) Create and activate virtual environnement
```bash
cd draft-server
python3 -m venv venv
```

* Activate the environnement on Windows PowerShell
```powershell
.\venv\Scripts\activate
```

* Activate the environnement on Linux
```bash
source venv/bin/activate
```

5) Install dependencies on virtual environnement
```bash
python -m pip install --upgrade pip
pip install -r requirements.txt
```


### Post installation

Now you can either run the server by launching app/server.py
or get a conf file for cubecobra hosted cube.

Dont forget to edit server.py by entering info on your server if you want href
to be correct on your pages.
Default is
```python
HOST="127.0.0.1" # Your server domain or ip address
PORT="5000"        # Server Port
PROTO="http"     # Or https if your have a SSL certificat
```


## Usage

### Start a draft

* On Windows PowerShell
```powershell
$json = Get-Content ./conf/config.json
Invoke-WebRequest -uri "http://127.0.0.1:5000/api/start_draft" -ContentType 'application/json' -Method POST -Body $json
```

* On Linux
```bash
curl -H "Content-Type: application/json" --data @conf/config.json http://127.0.0.1:5000/api/start_draft
```

### Get a configuration file

Replace re2 and 9ye in this command with your cubecobra ids
```bash
python app/cubecobra.py re2 9ye > conf/config.json
```


### Edit a configuration file:

The syntaxe for a Card object is the following:
```json
{
    "name": "Booster Tutor",
    "set_code": "unh",
    "url": "https://cards.scryfall.io/normal/front/c/3/c3a5b25e-c5af-417e-8675-162ab18ac2b1.jpg?1595010993",
    "associated_cards": []
}
```

- set_code is the three letter code of the card expansion
- url is for the card image
- associated_cards is a list of Card object auto-included when a player pick
  the main Card

The syntaxe for a Turn object is the following:
```json
{
    "cube_id": 0,
    "order": 1,
    "booster_size": 15
}
```

- cube_id represent the id of the cube you want cards from for this turn
- to add a turn, edit this tag and append it to the turns list.


## TODO

- [ ] Use a production web server (see https://gunicorn.org/#quickstart)
- [ ] Add neighbours info
- [ ] Implement in-draft statistics (start by ordering pool by cmc)
- [ ] Handle logging
- [ ] Tester le cas ou il y'a plus de boosters que de joueurs
- [ ] Add doc to edit conf file to the README
