var socket = io();
var selected_card = null;

socket.on('draft_state', function(data) {
    console.log(data);
    var stat = document.getElementById("status");
    while (stat.firstChild) {
        stat.removeChild(stat.firstChild);
    }
    for (let i = 0; i < data.length; i++){
        var delim = document.createElement("label");
        delim.innerHTML = " \u279c ";
        var mes = document.createElement("label");
        mes.innerHTML = data[i].player + " (" + data[i].boosters + ")";
        if (data[i].status){
            mes.className = "connected";
        } else {
            mes.className = "disconnected";
        }
        stat.appendChild(mes);
        console.log(i);
        console.log(data.length)
        if (i != data.length - 1){
            stat.appendChild(delim);
        }
    }
});

socket.on('connect', function() {
    socket.emit('join', {draft: draft_id, player: player_uid});
});

socket.on("player", function(player){
    // Remove old cards if there any
    var draft_content = document.getElementById("draft");
    while (draft_content.firstChild) {
        draft_content.removeChild(draft_content.firstChild);
    }
    if (player.current_booster){
        pending_cards = player.current_booster.cards;
        for (let i = 0; i < pending_cards.length; i++) {
            var cardTag = getDraftCardTag("draftCard" + i, pending_cards[i]);
            if (i == 0){
                // Set first card as the default selection and update css
                selected_card = cardTag;
                cardTag.className = "draftCardContainerSelected";
            }
            cardTag.addEventListener("click", function(){
                if (this.className == "draftCardContainerSelected"){
                    console.log(player.name + " choice emit "+ i);
                    // Remove selection for this card
                    selected_card.className = "draftCardContainer";
                    // Send info to server
                    socket.emit('choice', {"draft": player.draft_id, "player": player.uid, "choice": i});
                } else{
                    // Remove previous selection
                    selected_card.className = "draftCardContainer";
                    // selected this card by updating its css class
                    selected_card = this;
                    this.className = "draftCardContainerSelected";
                }
            })
            document.getElementById("draft").appendChild(cardTag);
        } 
    } else {
        // display waiting message
        var mes = document.createElement("p");
        mes.id = "no_booster";
        mes.style.fontWeight = 'bold';
        if (player.draft_state == "ended"){
            mes.innerHTML = "Le draft est terminé. ";
            var link = document.createElement("a");
            link.href = player.deckstat;
            link.innerHTML = "Retrouve ton deck en cliquant ici";
            mes.appendChild(link);
            
        } else {
            mes.innerHTML = "Pas de booster pour le moment";
        }
        document.getElementById("draft").appendChild(mes);
        console.log("no booster for the moment");
    }
    // Remove old cards if there any
    var pool_content = document.getElementById("pool");
    while (pool_content.firstChild) {
        pool_content.removeChild(pool_content.firstChild);
    }
    card_pool = player.pool;
    if (card_pool){
        for (let i = 0; i < card_pool.length; i++) {
            var cardTag = getCardTag("poolCard" + i, card_pool[i], "poolCard");
            document.getElementById("pool").appendChild(cardTag);
        } 
    }
});

socket.on("update", function(data){
    if (data.end_turn){
        // If end of turn, update all clients
        socket.emit('get player', {"draft": draft_id, "player": player_uid});
    } else if (player_uid == data.from){
        // else only update current player and is neighbour
        socket.emit('get player', {draft: draft_id, player: player_uid});
    } else if (player_uid == data.neighbour && document.getElementById("no_booster")){
        // else only update current player and is neighbour
        socket.emit('get player', {draft: draft_id, player: player_uid});
    } 
});

socket.on("end", function(data){
    console.log("Draft is over");
    socket.emit('get player', {draft: draft_id, player: player_uid});
});
