function isMobileDevice() {
    // Return true if mobile device
    return ( ( window.innerWidth <= 800 ) && ( window.innerHeight <= 600 ) );
}

function getCorrectImageSize(url){
    // Change image size depending on screen size
    if (url == null) return "";
    if (isMobileDevice()){
        return url.replace("/normal/", "/small/");
    } else {
        return url.replace("/small/", "/normal/");
    }
}

function getDraftCardTag(id, card){
    var container = document.createElement("span");
    container.id = id + "container";
    container.className = "draftCardContainer";
    container.appendChild(getCardTag(id, card, "draftCard"));
    for (let i = 0; i < card.associated_cards.length; i++) {
        var asso = document.createElement("a");
        asso.innerHTML = "Lié à " + card.associated_cards[i].name;
        asso.href = card.associated_cards[i].url;
        asso.target = "_blank";
        asso.className = "associatedCard";
        container.appendChild(asso);
    }
    return container;
}

function getCardTag(id, card, css){
    // TODO: create onhover preview with associated cards
    var img = document.createElement("img");
    img.id = id;
    img.alt = card.name;
    img.src = getCorrectImageSize(card.url);
    img.className = css;
    return img;
}
