from __future__ import annotations
import os
import json
import uuid
from deckstats.deckstats import Deck as DeckstatsDeck
from scryfall.Scryfall import Scryfall
from cubecobra import CubeCobra
from typing import List, Dict, Union, Any
from enum import Enum
import random
from datetime import datetime, date
from tqdm import tqdm
import re
from sqlalchemy import create_engine, Column, Integer, String, DateTime, Text, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, reconstructor

engine = create_engine(
    f"sqlite:///{os.path.dirname(os.path.abspath(__file__))}/db/draft.db",
    connect_args={"check_same_thread": False},
)
Base = declarative_base()


class DraftState(Enum):
    INIT = "initiated"
    PREP = "prepared"
    START = "started"
    END = "ended"


class Card:

    def __init__(
        self,
        name: str,
        set_code: str = None,
        url: str = None,
        associated_cards: List[Card] = [],
    ):
        self.name = name
        self.set_code = set_code
        self.url = url
        self.associated_cards = [Card(**c) for c in associated_cards]

    def to_dict(self) -> dict:
        """
        Get object with public attributes to json form.
        :return: dict
        """
        return {
            "name": self.name,
            "set_code": self.set_code,
            "url": self.url,
            "associated_cards":
            [card.to_dict() for card in self.associated_cards],
        }


class Player:

    def __init__(self, name: str = "", uid: int = 0):
        self.name = name
        self.uid = uid
        self.draft: Draft
        self.pending_boosters: List[Booster] = []
        self.pool: List[Card] = []
        # To store pool url on deckstat
        self.deckstat: str = ""
        self.current_choice: Choice = None
        self.previous_picks: List[Choice] = []

    def choose_card(self, index: int) -> Any[True, None]:
        current_booster = self.get_current_booster()
        if current_booster and index < len(current_booster.cards):
            self.current_choice = Choice(player=self,
                                         draft=self.draft,
                                         index=index)
            return True

    def add_card_to_pool(self, card: Card) -> None:
        """
        Add card and all associated cards to player's pool
        :param card: Card
        :return: None
        """
        self.pool.append(card)
        self.pool += card.associated_cards

    def check_for_last_card(self) -> None:
        """
        If auto_last_pick is set, check if current booster contains only one card if so pick it
        :return: None
        """
        if self.draft.auto_last_pick:
            current_booster = self.get_current_booster()
            if current_booster and len(current_booster.cards) == 1:
                self.current_choice = Choice(player=self,
                                             draft=self.draft,
                                             index=0)
                self.pick_card()

    def add_booster(self, booster: Booster):
        """
        Add booster to player booster stack.
        :param booster: Booster
        :return: None
        """
        self.pending_boosters.append(booster)
        self.check_for_last_card()

    def pass_current_booster(self) -> Any[True, None]:
        """
        check if player a has booster, if so remove it and give it to neighbour.
        If the next booster contains only one card and auto_last_pick is on, perform another pick
        :return: True if action performed, None otherwise
        """
        if self.pending_boosters:
            booster = self.pending_boosters.pop(0)
            self.draft.get_neighbour(self).add_booster(booster)
            self.check_for_last_card()
            return True

    def pick_card(self) -> Any[Card, None]:
        """
        Should check if action is correct, then take the card, then update choice
        :param persist: Boolean to choose if we want to save or not
        :return: Card or None
        """
        # Check if player has a booster and made a choice
        current_booster = self.get_current_booster()
        if self.current_choice is not None and current_booster:
            # check then if the booster have a card left
            if picked_card := current_booster.pick_card_by_index(
                    self.current_choice.index, player=self):
                self.add_card_to_pool(picked_card)
                self.current_choice.turn = self.draft.current_turn_index + 1
                self.current_choice.pick = len(self.previous_picks) + 1
                self.previous_picks.append(self.current_choice)
                if self.draft.persist:
                    session.add(self.current_choice)
                    session.commit()
                self.current_choice = None
                # Check if Booster is empty, delete it
                if len(current_booster.cards) == 0:
                    del self.pending_boosters[0]
                else:
                    self.pass_current_booster()
                return picked_card

    def get_current_booster(self) -> Booster:
        """
        Get current booster to pick card from
        :return: Booster or None
        """
        if self.pending_boosters:
            return self.pending_boosters[0]

    def empty_pending_boosters(self) -> None:
        """
        Remove all boosters from pending booster list
        :return: None
        """
        self.pending_boosters = []

    def to_dict(self) -> dict:
        """
        Get object with public attributes to json form.
        :return: dict
        """
        if current_booster := self.get_current_booster():
            current_booster_dict = current_booster.to_dict()
        else:
            current_booster_dict = None
        return {
            "name": self.name,
            "uid": self.uid,
            "pool": [card.to_dict() for card in self.pool],
            "current_booster": current_booster_dict,
            "current_choice": self.current_choice,
            "draft_id": self.draft.id,
            "draft_state": self.draft.state,
            "deckstat": self.deckstat,
        }

    def __repr__(self):
        return json.dumps(self.to_dict())


class Draft(Base):

    __tablename__ = "draft"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime, default=datetime.now())
    seed = Column(Float, default=datetime.now().timestamp())
    state = Column(String)
    conf = Column(Text)

    def __init__(self, conf: dict):

        self.conf = json.dumps(conf)
        if conf.get("seed"):
            self.seed = conf.get("seed")
        else:
            self.seed = datetime.now().timestamp()
        self.randomizer = random.Random()
        self.randomizer.seed(self.seed)
        self.cubes = [Cube(**cube) for cube in conf.get("cubes")]
        self.picks: List[Pick] = []
        self.current_turn_index: int = 0
        self.pick_index: int = 0  # each pick increment this number
        self.seed = None
        self.conf = json.dumps(conf)

        # Parameters
        self.auto_last_pick = conf.get("auto_last_pick")
        self.sync_picks = conf.get("sync_picks")
        self.alternated_turn_order = conf.get("alternated_turn_order")
        self.turn_order = 1
        self.persist = True
        self.starting_cards = []

        try:
            self.starting_cards = conf.get("starting_cards")
        except Exception as e:
            print(e)

        self.players = []
        for player in [Player(**player) for player in conf.get("players")]:
            self.add_player(player)

        self.turns = []
        for turn in [Turn(**turn) for turn in conf.get("turns")]:
            self.add_turn(turn)

        self.state = DraftState.INIT.value

    @reconstructor
    def __init_on_load__(self):
        """Constructor from database infos"""
        conf = json.loads(self.conf)
        self.randomizer = random.Random()
        self.randomizer.seed(self.seed)
        self.cubes = [Cube(**cube) for cube in conf.get("cubes")]
        self.picks: List[Pick] = []
        self.current_turn_index: int = 0
        self.pick_index: int = 0  # each pick increment this number
        self.auto_last_pick = conf.get("auto_last_pick")
        self.sync_picks = conf.get("sync_picks")
        self.alternated_turn_order = conf.get("alternated_turn_order")
        self.turn_order = 1
        # Remove persistant action during loading
        self.persist = False
        try:
            self.starting_cards = Cube.parse_cards_from_txt(
                conf.get("starting_cards"))
        except Exception as e:
            print(e)
        self.players = []
        for player in [Player(**player) for player in conf.get("players")]:
            self.add_player(player)
        self.turns = []
        for turn in [Turn(**turn) for turn in conf.get("turns")]:
            self.add_turn(turn)
        self.state = DraftState.INIT.value
        self.start()

        # Replay players choices
        choices = session.query(Choice).filter(
            Choice.draft_id == self.id).all()
        for choice in choices:
            player = self.get_player_by_name(choice.player_name)
            self.add_choice(choice.index, player)

        # Once loading is done we persist new changes
        self.persist = True

    def start(self) -> None:
        """
        Verify if draft is correctly prepared, if yes append a booster to each
        player and change draft state.
        :return: None
        """
        if self.state == DraftState.INIT.value:
            self.prepare()
        for i, player in enumerate(self.players):
            player.pending_boosters.append(self.get_current_turn().boosters[i])
            self.state = DraftState.START.value
        if self.persist:
            session.add(self)
            session.commit()

    def prepare(self) -> None:
        """
        Shuffle cards and players, load Turns with boosters then change draft state
        :return: None
        """
        [self.randomizer.shuffle(cube.cards) for cube in self.cubes]
        self.randomizer.shuffle(self.players)
        for turn in self.turns:
            cube = self.get_cube_by_id(turn.cube_id)
            for _ in self.players:
                b = Booster(
                    cards=[cube.cards.pop() for _ in range(turn.booster_size)])
                if len(b.cards) < turn.booster_size:
                    print("Not enough cards for that many players.")
                    raise Exception("Not enough cards for that many players.")
                turn.boosters.append(b)
        self.state = DraftState.PREP.value

    def stop(self) -> None:
        """Stop the draft and create deckstat url to view player pool online"""
        self.state = DraftState.END.value
        print("======== Draft is over ========")
        timestamp = date.today().strftime("%d-%m-%Y")
        for player in self.players:
            title = f"Draft de {player.name} du {timestamp}"
            deck = DeckstatsDeck(cards=[c for c in player.pool], title=title)
            player.deckstat = deck.get_url()

    def add_player(self, player: Player) -> None:
        """
        Attach a player to the Draft and give him his starting cards.
        :param player: Player
        :return: None
        """
        player.draft = self
        player.uid = str(
            uuid.UUID(int=self.randomizer.getrandbits(128), version=4))
        self.players.append(player)
        if self.starting_cards:
            player.pool += self.starting_cards

    def add_turn(self, turn: Turn) -> None:
        """
        Attach a Turn to the Draft and synchronized its turn_order if specified.
        :param turn: Turn
        :return: None
        """
        turn.draft = self
        if self.alternated_turn_order:
            turn.order = -self.turn_order
        self.turns.append(turn)

    def add_choice(self, choice: int, player: Player) -> bool:
        """
        Function for player to emit choices during the draft.
        Check if the choice is valid
        Pick is valid: save choice if sync_pick else pick card and move booster
        Then update draft state
        Pick is invalid, return None

        :param choice: card_index in player's current booster
        :param player: Player who choose the card
        :return: bool False if incorrect choice
        """
        if not self.state == DraftState.START.value:
            return False

        valid_choice = player.choose_card(index=choice)
        if not valid_choice:
            print("Invalid choice")
            return False
        if self.sync_picks:
            # Test if all players made a choice, if so pick each player pick
            if all([
                    isinstance(p.current_choice, Choice) for p in self.players
                    if p.get_current_booster()
            ]):
                for p in self.players:
                    p.pick_card()
        else:  # Sync_pick is off
            player.pick_card()

        # After a valid choice, we check draft state
        current_turn = self.get_current_turn()
        if current_turn.is_over():
            print(f"Turn {self.current_turn_index} is over")
            # Turn is over move to next one
            current_turn.stop()
            self.current_turn_index += 1
            if current_turn := self.get_current_turn():
                print(f"Start turn {self.current_turn_index}")
                # There is another turn left, start it
                current_turn.start()
            else:
                # Draft is over
                self.stop()
        return True

    def get_current_turn(self) -> Turn:
        """
        Return current Turn object
        """
        if self.current_turn_index < len(self.turns):
            return self.turns[self.current_turn_index]

    def get_neighbour(self, player: Player) -> Player:
        """
        Get neighbour of given player
        :param player: Player
        :return: Player (neighbour)
        """
        if self.get_current_turn():
            return self.players[(self.players.index(player) +
                                 self.get_current_turn().order) %
                                len(self.players)]

    def get_cube_by_id(self, id: int) -> Union[Cube, None]:
        """
        Get Cube by its id number.
        :param id: int
        :return: Cube else return None
        """
        for cube in self.cubes:
            if cube.id == id:
                return cube

    def get_player_by_name(self, name: str):
        for player in self.players:
            if player.name == name:
                return player

    def get_player_by_uid(self, uid: int):
        for player in self.players:
            if player.uid == uid:
                return player


class Choice(Base):

    __tablename__ = "choices"

    id = Column(Integer, primary_key=True)
    draft_id = Column(String, primary_key=True)
    player_name = Column(String, primary_key=True)
    index = Column(Integer, primary_key=True)  # card rank in booster
    date = Column(DateTime, default=datetime.now())

    def __init__(self, player: Player, draft: Draft, index: int):
        self.id = draft.pick_index
        self.draft_id = draft.id
        self.player_name = player.name
        self.index = index
        self.date = datetime.now()
        draft.pick_index += 1


class Turn:
    """Link Players with Boosters"""

    def __init__(self,
                 cube_id: int = 0,
                 order: int = 1,
                 booster_size: int = 15):
        self.draft: Draft
        self.boosters: List[Booster] = []
        self.cube_id = cube_id
        self.order = order
        self.booster_size = booster_size

    def start(self) -> None:
        """
        Give each player a booster.
        :return: None
        """
        for i, booster in enumerate(self.boosters):
            self.draft.players[i].pending_boosters.append(booster)

    def stop(self) -> None:
        """
        Remove booster from each player.
        :return: None
        """
        (player.empty_pending_boosters() for player in self.draft.players)

    def is_over(self) -> bool:
        """
        Check if all boosters are empty.
        :return: True if all boosters are empty, else return False
        """
        return all([len(booster.cards) == 0 for booster in self.boosters])

    def add_booster(self, booster: Booster, player: Player = None) -> None:
        """
        Add booster to the Turn and append it to player if specified
        :param booster: Booster
        :param player: Player
        :return: None
        """
        self.boosters.append(booster)
        if player and len(booster.cards) == self.booster_size:
            player.pending_boosters.append(booster)


class Pick:

    def __init__(self, card_number: int, player: Player):
        self.player = player
        self.card_number = card_number
        self.timestamp = datetime.now()


class Booster:

    def __init__(self, cards=[]):
        self.id = random.randint(0, 100)
        self.cards: List[Card] = cards
        self.last_pick_player: Player = None

    def pick_card_by_index(self,
                           index: int,
                           player: Player = None) -> Union[Card, None]:
        """
        Remove a card from Booster given by its index and return that card.
        If a player is specified, update last_picked_player
        :param index: int
        :param player: Player
        :return: Card
        """
        if index < len(self.cards):
            if player:
                self.last_pick_player = player
            return self.cards.pop(index)

    def add_card(self, card: Card) -> None:
        self.cards.append(card)

    def to_dict(self) -> dict:
        """
        Get object with public attributes to json form.
        :return: dict
        """
        return {
            "id":
            self.id,
            "last_pick_player":
            self.last_pick_player.name if self.last_pick_player else None,
            "cards": [card.to_dict() for card in self.cards],
        }

    def __repr__(self):
        return json.dumps(self.to_dict())


class Cube:

    def __init__(self,
                 cards: Union[List[Card], str],
                 url: str = None,
                 id: int = 0):
        self.cards = [Card(**c) for c in cards]
        self.url = url
        self.id = id

    def import_cards_from_cubecobra(self,
                                    cubecobra_id: str,
                                    fetch_image: bool = True) -> None:
        """
        Get list of cards from CubeCobra
        :param cube_id: str Cube code from CubeCobra
        :param fetch_image: bool if true, get image_url from scryfall
        :return: None
        """
        for card in tqdm(CubeCobra.get_cube_list(cube_id=cubecobra_id)):
            c = Card(name=card["Name"], set_code=card["Set"])
            if url := card.get("Image URL"):
                c.url = url
            elif fetch_image:
                card = Scryfall().get_card(exact=card["Name"], set=card["Set"])
                if card:
                    c.url = card.get_images_url()[0]
            self.cards.append(c)

    @staticmethod
    def parse_cards_from_txt(txt: str) -> List[Card]:
        """
        Import cards by converting str to list of Card.
        cards must be separated with \n
        :param txt: str List of cards separated with \n
        :return: None
        """
        return [Card.from_str(c) for c in txt.splitlines()]

    def get_raw_config(self) -> dict:
        """
        Get raw cube config for this cube (without turn_order...)
        Used for draft config
        """
        return {
            "cards": [c.to_dict() for c in self.cards],
            "url": self.url,
            "id": 0
        }


def get_draft_conf_example() -> dict:
    conf = {}
    conf["cubes"] = []
    conf["turns"] = [
        {
            "cube_id": 0,
            "order": 1,
            "booster_size": 15
        },
        {
            "cube_id": 0,
            "order": 1,
            "booster_size": 15
        },
        {
            "cube_id": 0,
            "order": 1,
            "booster_size": 15
        },
    ]
    conf["players"] = [
        {
            "name": "nico"
        },
        {
            "name": "greg"
        },
        {
            "name": "remi"
        },
        {
            "name": "gauthier"
        },
        {
            "name": "romain"
        },
    ]
    conf["auto_last_pick"] = True
    conf["sync_picks"] = False
    conf["alternate_turn_order"] = True
    conf["seed"] = ""
    conf[
        "starting_cards"] = "Command Tower [CMD] (https://c1.scryfall.com/file/scryfall-cards/small/front/7/c/7cf39d35-1426-4edb-98ae-d1f1a470629f.jpg?1663451121)"
    return conf


def load_conf_file(file="../conf/config.json") -> Draft:
    with open(file, "r") as f:
        conf = json.loads(f.read())
    return Draft.from_json(conf)


Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()
