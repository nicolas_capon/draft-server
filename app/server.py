#!../venv/bin/python3.10
import json
import model
from flask import Flask, request, jsonify, render_template
from flask_socketio import SocketIO, emit

HOST = "serveurnico.synology.me"
PROTO = "http"
PORT = 5000

app = Flask(__name__)
socketio = SocketIO(app)
drafts = []


def get_draft_by_id(id: int) -> model.Draft:
    for draft in drafts:
        if draft.id == id:
            return draft


def get_draft_link(draft: model.Draft, player: model.Player) -> str:
    return f"{PROTO}://{HOST}:{PORT}/gui/draft?player={player.uid}&draft={draft.id}"


@socketio.on("get player")
def send_player(data):
    draft = get_draft_by_id(int(data["draft"]))
    if draft:
        player = draft.get_player_by_uid(data["player"])
        if player:
            emit("player", player.to_dict())


@socketio.on("choice")
def handle_player_choice(data):
    print("socket choice:", data)
    draft = get_draft_by_id(int(data["draft"]))
    player = draft.get_player_by_uid(data["player"])
    if not draft:
        return "Wrong draft id", 400, None
    choice = data["choice"]
    if player and isinstance(choice, int):
        turn_ind = draft.current_turn_index
        draft.add_choice(player=player, choice=choice)
        if neighbour := draft.get_neighbour(player):
            emit(
                "update",
                {
                    "end_turn": not turn_ind == draft.current_turn_index,
                    "from": player.uid,
                    "neighbour": neighbour.uid,
                },
                broadcast=True,
            )
        else:
            # Draft is over, send END signal
            emit("end", broadcast=True)
    else:
        print("WRONG MESSAGE")


@app.route("/gui/draft")
def get_draft_grid():
    """
    endpoint for drafting web app
    url must be like /gui/draft?player=bot
    """
    try:
        draft_id = int(request.args.get("draft"))
    except ValueError as e:
        draft_id = None
        return e, 422, None
    draft = get_draft_by_id(draft_id)
    if not draft:
        return "Invalid draft id", 422, None
    player = draft.get_player_by_uid(request.args.get("player"))
    if not player:
        return "Invalid player", 422, None
    return render_template("draft.html", player=player, draft=draft)


@app.route("/api/start_draft", methods=["POST"])
def start_draft():
    conf = request.get_json()
    # Try to create draft from conf file
    try:
        draft = model.Draft(conf)
    except Exception as e:
        print(e)
        return "Something went wrong with this config file", 400, None

    # Try to start the draft
    try:
        draft.start()
    except IndexError as e:
        return "Not enough cards for that many players", 400, None
    except Exception as e:
        print(e)
        return "Something went wrong with this config file", 400, None

    # If draft is ok, append it
    drafts.append(draft)
    return "OK\n", 200, None


@app.route("/api/drafts")
def get_players_drafts():
    """Return list of all players with their current drafts"""
    drafts_data = {"drafts": []}
    for draft in drafts:
        d_data = {
            "id": draft.id,
            "seed": draft.seed,
            "date": draft.date,
            "players": []
        }
        for player in draft.players:
            p_data = {
                "name": player.name,
                "uid": player.uid,
                "draft_link": get_draft_link(draft, player),
            }
            d_data["players"].append(p_data)
        drafts_data["drafts"].append(d_data)
    return jsonify(drafts_data), 200, None


if __name__ == "__main__":
    # Reload not finished drafts
    db_drafts = model.session.query(model.Draft).filter(model.Draft.state != model.DraftState.END.value).all()
    for d in db_drafts:
        drafts.append(d)
    socketio.run(app, host="0.0.0.0", port=PORT, allow_unsafe_werkzeug=True)
