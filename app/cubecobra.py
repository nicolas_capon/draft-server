import os
import requests
import logging
import csv
from typing import List


class CubeCobra:

    url = "https://cubecobra.com"

    @classmethod
    def get_cube_list(cls, cube_id: str) -> List:
        url = os.path.join(cls.url, "cube/download/csv", cube_id)
        r = requests.get(url)
        if r.ok:
            data = r.text
            return list(csv.DictReader(data.splitlines()))
        else:
            logging.info(
                f"[{r.status_code}] Could not fetch cube list from {url}")


if __name__ == "__main__":
    import sys
    import json
    from model import Cube, get_draft_conf_example

    config = get_draft_conf_example()
    cubecobra_ids = sys.argv[1:] # First arg is script name
    for n, cubecobra_id in enumerate(cubecobra_ids):
        cube = Cube(
            cards=[],
            url=f"{CubeCobra.url}/cube/overview/{cubecobra_id}",
            id=n,
        )
        cube.import_cards_from_cubecobra(cubecobra_id=cubecobra_id)
        config["cubes"].append(cube.get_raw_config())
    print(json.dumps(config, indent=4))
